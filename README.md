# Curso React: De cero a experto ( Hooks y MERN )

## Proyecto Hooks App (Test)

Instruido por:

Fernando Herrera

## Nota:

creado con:

`npx create-react-app`

### Comandos:
    
* `npm install --save react-router-dom`
* `npm install --save-dev @testing-library/react-hooks`
* `npm install --save-dev enzyme`
* `npm install --save-dev @wojtekmaj/enzyme-adapter-react-17`
* `npm install --save-dev enzyme-to-json`
 